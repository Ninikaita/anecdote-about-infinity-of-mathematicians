#!/bin/python3

import sys
import time

phys = False

sleep = 0.5

i = 1
while (i < len(sys.argv)):
	if (sys.argv[i] == "-h"):
		print("usage: {} [-h] [-p/--phys] [time in seconds]".format(sys.argv[0]))
		exit(0)
	elif (sys.argv[i] in ('-p','--phys')):
		phys = True
	else:
		try:
			sleep = float(sys.argv[i])
		except:
			print("Неверное число")
			exit(1)
	i += 1

if (phys):
	print("Заходит в бар бесконечность физиков.")
else:
	print("Заходит в бар бесконечность математиков.")
time.sleep(sleep)

n = 1
powered = 2**(n - 1)

def printline(n):
	if (n == 1):
		print("{}-й заказывает 1 кружку пива.".format(n))
	else:
		powered *= 2
		print("{}-й заказывает 1/{} кружку пива.".format(n,powered))

if (phys):
	#1.6855999999999998e+22 is approximate limit of denominator, when beer isn't pure beer because of atom properties in usual beer mug.
	while n < 74:
		printline(n)
		n += 1
		time.sleep(sleep)
else:
	while True:
		printline(n)
		n += 1
		time.sleep(sleep)

print('Бармен отвечает им "Тьфу, дураки!" и налил 2 кружки пива.')
